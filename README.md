# Ethereum Validator Stack

This project provides a Docker Compose setup for running an Ethereum validator using Prysm and Geth.

## Background

### Execution Layer and Consensus Layer

With the introduction of Ethereum 2.0, the Ethereum blockchain has been divided into two main components:

- **Execution Layer**: This is essentially Ethereum integrated as one of many shard chains.

- **Consensus Layer**: This is the new addition with Ethereum 2.0. It's responsible for coming to consensus on the state of the network, including the state of the execution layer. The consensus layer uses a Proof-of-Stake (PoS) mechanism, as opposed to the legacy Proof-of-Work (PoW) mechanism used by Ethereum.

### Validator

In the PoS mechanism of Ethereum 2.0, validators replace miners in the consensus mechanism. Validators propose and attest to blocks, and their influence is proportional to their stake. Validators need to be running a beacon node and a validator client. The beacon node communicates with the wider Ethereum network, while the validator client manages the validator's specific duties.

## Setup

### Prerequisites

- Docker and Docker Compose installed.
- An Ethereum address for receiving fees.
- Wallets configured according to https://ethereum.org/en/staking/

### Initial Setup

1. **Secrets Generation**: Before starting the services, you need to generate a JWT secret for authentication between the beacon node and the execution node (Geth). This can be done using OpenSSL:

   ```bash
   openssl rand -hex 32 | tr -d "\n" > ./secrets/jwt.hex
   ```

2. **Wallet Import**: If you already have validator keys, you can import them into the Prysm validator. Copy your `validator_keys` folder to the `wallets` directory in this project.

3. **Environment Variables**: Create a `.env` file in the root directory of this project and add the following line:

   ```env
   FEE_RECIPIENT_ADDRESS=your_ethereum_address_here
   ```

   Replace `your_ethereum_address_here` with your actual Ethereum address where you'd like to receive fees.

### Running the Stack

Navigate to the project directory and run:

```bash
docker-compose up -d
```

This will start the Geth execution client, the Prysm beacon node, and the Prysm validator client.

## Monitoring and Maintenance

Ensure to monitor the logs of your Docker containers to keep track of the synchronization process, potential errors, and the overall health of your validator.

---

Remember to always backup your data and secrets, and never expose sensitive ports or information.